# How to build SWAN on Windows 10

## Step 1. Install the compiler and building tools
SWAN is a software written in Fortran, so we need to install a Fortran compiler to translate the source code to an executable program.

As stated in [this page of the SWAN Implementation manual](http://swanmodel.sourceforge.net/online_doc/swanimp/node4.html) and on the *INSTALL.README* file of the SWAN source code, **the only supported compiler for building SWAN under Microsoft® Windows (64-bit) is the Intel® Fortran Compiler.**

We'll need:
* A **Perl** language interpreter to run source code scripts.
* The **Intel® Fortran Compiler** to compile the source code.
* **MPICH2** Package if we need SWAN in MPI mode (Optional).
* A `Makefile` building tool for Windows. I like **Jom** because it's open-sourced.

### Perl
Perl is an interpreted programming language used for scripting. The SWAN source code needs it to execute two scripts (`platform.pl` and `switch.pl`) which configure some compiler options before running.

The [official Perl website](https://www.perl.org/get.html) offers two different packages for Windows. Either package works correctly:
* [Strawberry Perl](http://strawberryperl.com/) (101MB)
* [ActiveState Perl](https://www.activestate.com/products/perl/downloads/) (5.5MB)

After install, test if it works by opening a terminal and call `perl --version`. It should return this:
```bash
This is perl 5, version 28, subversion 1 (v5.28.1) built for MSWin32-x64-multi-thread
(with 1 registered patch, see perl -V for more detail)

Copyright 1987-2018, Larry Wall

Binary build 0000 [b720e951] provided by ActiveState http://www.ActiveState.com
Built Sep 14 2020 17:43:01

Perl may be copied only under the terms of either the Artistic License or the
GNU General Public License, which may be found in the Perl 5 source kit.
```

### Intel® Fortran Compiler
The Intel® Fortran compiler used to be shipped within the **Intel® Parallel Studio XE package**, but they've moved it to the new **Intel® oneAPI Toolkits**. You can get it for free in the [free Intel® Software Development Tools website](https://software.intel.com/content/www/us/en/develop/articles/qualify-for-free-software.html). You need to download:

* [Intel® oneAPI Base Toolkit](https://software.intel.com/content/www/us/en/develop/tools/oneapi/base-toolkit/download.html) (3.3GB). This is the base system.
* [Intel® oneAPI HPC Toolkit](https://software.intel.com/content/www/us/en/develop/tools/oneapi/hpc-toolkit/download.html) (1.0GB). This toolkit includes:
  * Intel® oneAPI DPC++/C++ Compiler & Intel® C++ Compiler Classic
  * Intel® Fortran Compiler (Beta) & **Intel® Fortran Compiler Classic (*ifort*)**
  * Intel® MPI Library

Install the base toolkit first, then the HPC toolkit. 

### MPICH2 (Optional)
MPICH2 is needed when compiling SWAN in MPI mode (i.e. to use it in a computer cluster or on a single computer with multiple processors). The last MPICH2 version with Windows support was **1.4.1p1** ([There's why](https://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions#Q:_Why_can.27t_I_build_MPICH_on_Windows_anymore.3F)).

Get the *mpich2-1.4.1p1-win-x86-64.msi* package (9.4MB) from [the official site](https://www.mpich.org//static/downloads/1.4.1p1/) and install it.

Once installed, add `C:\Program Files\MPICH2\bin` to the [PATH variable](https://stackoverflow.com/questions/44272416/how-to-add-a-folder-to-path-environment-variable-in-windows-10-with-screensho).

### Jom
Jom is a clone of nmake, a command line similar to GNU make.

Get *jom.zip* (1.2MB) from [this link](http://download.qt.io/official_releases/jom/jom.zip) and unzip it on a desired location, i.e. `C:\`

Once installed, add `C:\jom_1_1_3` to the [PATH variable](https://stackoverflow.com/questions/44272416/how-to-add-a-folder-to-path-environment-variable-in-windows-10-with-screensho). Test if it works by opening a terminal and call `jom /VERSION`. It should return this:
```bash
jom version 1.1.3
```


## Step 2. Download the source code
First, open a new command prompt and enable the building environment with the following command:
```bash
"C:\Program Files (x86)\Intel\oneAPI\setvars.bat" intel64
```
Using the same command prompt, I like to create a new folder to work, for example, `C:\swan`:
```bash
mkdir "C:\swan"
```
And then:
```bash
cd "C:\swan"
```
Once in, we use `curl` command to download SWAN:
```bash
curl -OJ http://swanmodel.sourceforge.net/download/zip/swan4131.tar.gz
```
We need to extract the files before building. Most common procedure is to use the `tar` command with `-x`, `-z` and `-f` options:
```bash
tar -xzf swan4131.tar.gz
```
Now all files were extracted to a new folder called `swan4131`. Let's go there:
```bash
cd swan4131
```


## Step 3. Build
Here's the point where the procedure diverges depending on which mode do you want to run SWAN:

* **Serial**: for computers only using one processor (like your PC).
* **Parallel, shared (OMP)**: for shared memory systems (like your PC, using multiple processors).
* **Parallel, distributed (MPI)**: for distributed memory systems (like clusters).

**Follow just one of the following (A, B or C) instructions!**

### A - Serial mode
Configure SWAN for Windows/DOS systems by executing the `switch.pl` Perl script with `-dos` flag:
```bash
perl switch.pl -dos *.ftn *.ftn90
```
Next, generate a config file based on your system:
```bash
jom config
```
Ready to build SWAN. To do it in serial mode, type:
```bash
jom ser
```

### B - Parallel, shared mode (OMP)
Configure SWAN for Windows/DOS systems by executing the `switch.pl` Perl script with `-dos` flag:
```bash
perl switch.pl -dos *.ftn *.ftn90
```
Next, generate a config file based on your system:
```bash
jom config
```
Ready to build SWAN. To do it in parallel-shared mode, type:
```bash
jom omp
```
Note that `swan.exe` compiled in OMP mode has a dependency with the dynamic link library `libiomp5md.dll`. To run this executable on another computer, it's necessary to download and install the [Intel® Fortran Compiler Redistributable Libraries](https://software.intel.com/content/www/us/en/develop/articles/intel-compilers-redistributable-libraries-by-version.html), matching the compiler version used when building.

### C - Parallel, distributed mode (MPI)
Configure SWAN for Windows/DOS systems by executing the `switch.pl` Perl script with `-dos` and `-mpi` flags:
```bash
perl switch.pl -dos -mpi *.ftn *.ftn90
```
Next, generate a config file based on your system:
```bash
jom config
```
You have to make a decision: 
* To use the MPICH2 library, don't touch anything.
* To use the Intel® MPI Library, you need to modify the `macros.inc` file as follows:
  * Set `F90_MPI = ifort` to `F90_MPI = mpiifort`.
  * Set `INCS_MPI = /include:"C:\PROGRA~1\MPICH2\include"` to `INCS_MPI =` (leave empty).
  * Set `LIBS_MPI = C:\PROGRA~1\MPICH2\lib\fmpich2.lib` to `LIBS_MPI =` (leave empty).

Finally, build SWAN in parallel-distributed mode by typing:
```bash
jom mpi
```

* If you compiled with the MPICH2 Library, `swan.exe` has a dependency with the dynamic link library `fmpich2.dll`. To run this executable on another computer, it's necessary to download and install [MPICH2](https://www.mpich.org//static/downloads/1.4.1p1/) on that computer.

* If you compiled with the Intel® MPI Library, `swan.exe` has a dependency with the dynamic link library `impi.dll`. To run this executable on another computer, it's necessary to download and install the [Intel® Fortran Compiler Redistributable Libraries](https://software.intel.com/content/www/us/en/develop/articles/intel-compilers-redistributable-libraries-by-version.html), matching the compiler version used when building.


## Step 4. Finish
If no errors were shown during the building process and you find `swan.exe` and `swanrun.bat` files inside your working folder, you've successfully built SWAN.

If you add that folder (`C:\swan\swan4131`) to [the PATH variable](https://stackoverflow.com/questions/44272416/how-to-add-a-folder-to-path-environment-variable-in-windows-10-with-screensho), you can run SWAN by calling `swanrun -input [filename]` from anywhere in your computer.